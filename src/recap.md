# Recap - Did we get started ?
As I said before , Gita not only provides the axioms, it also gives you the framework . A framework is needed because "Actions without Desires" are kinda tricky. You start on them and soon you realize that desires catch up. Soon you find yourself craving the pleasures. And there are pitfalls such as a community of admirers. 

Before we go to the Framework, lets quickly recap what we covered in "Getting Started Section" .. 

- We have [protected] knowledge that is distributed across all the living entities as a full copy and acts in a federated model.
- Collective consciousness does all the work. It only understands the language of "Actions". 
- There are millions of templates of regular action lists. We invoke them to get us started here. Templates include basic operations of the system such as rise of Sun to our survival kit. Most of them are automated - means you don't even need to make a choice. They act like demons if you are from Unix world. 
- Every engagement is packed with pleasure to get us hooked. Which is not a bad thing. We just need to watch that we are not caught in the pleasure traps. 
- We are given curiosity to explore this grand system. 
- We use the system to seek pleasures and pay back with pains and sorrow. Pain is kinda currency of this system. It is the pain that makes you look for exit ie make you solve the puzzle. Pain is good if you know how to use it for a purpose. 
- Finally, if we don't follow the code of conduct, there is mechanism to take away our privileges. Those include warnings to suspending our account as a human. That is BTW not an exit. That is actually a state of exponential misery. 
- The only exit is if we solve this maze. If we want to do it, there is online and context specific help available with you. 
- Gita suggests that only way to understand this maze is to understand and indulge in actions and practice - `Karma and Yajna` because the "Actions" are the language of this universe.  Once you know how to indulge in actions - you can have  this universe deliver anything. 
- However, even the biggest accomplishment, will fade in due course. Worldly success is NOT an exit in this game. 
- The way  to exit is to engage in "Actions without Desires" [AWDs]. Such actions are blissful. They give us lasting  peace in this world and exit when we leave. 

## Let me also leave you with a question - 

>> What could be a good action without desires. Should I close my eyes and just focus on breathing. Should I start watching the waves crashing on the shores of an ocean. Or should I continue on the work that I am currently doing and try to remove desires from it. Or is there something else that I need to explore .. 

## Let me also leave a clue 

>> If you know Mahabharata - Why did Krishna ask Arjuna to fight. Isn't that violence ? 

All those question and many more when we get to the framework and path forward. We are just getting started. There is lot to understand but having made thus far , rest should be easy .. And you will see your [protected]knowledge already talking to you  :-)  ..That makes it a fun ride 


