## Intelligent Design Vs Evolution 

We can argue [inconclusively] if the system created the modules -aka "Intelligent Design" or whether modules created the system aka "evolution". The goal is not to get into "chicken or egg " debate because that doesn't solve any question. Inconclusively , because we can not answer this question till we know all the answers. Thus the idea is to first understand what is currently going on - as of this very moment. If we don't understand the present, what is our chance to successfully speculate about past or future. We don't even know the exact nature of time. Then it is obviously beyond us to know about past. 

### The Circular nature of debate

The typical debate on Evolution vs Intelligent Design boils down to matter of faith. What exactly you believe in ?  For example Science camp would argue that the Religious camp has a blind faith in God , that God (or system) created everything with a pre-conceived design.  The Religion folks can argue exactly the same thing - that the other camp has a blind faith in experiments. And they may not be entirely wrong because all experiments finally start from a belief. We get to scientific experiments because someone had a "thought experiment" first. The It took many years and many wrong experiments before we could prove Einstein's thought experiment in a lab. The question (religious group) would ask - how did Einstein got the inspiration ? He didn't write the equations first. He first got an inspiration and then proved that inspiration through math. Basically he used math to prove his "Godly" inspiration to rest of us because that is the language other scientists understood. Einstein also got lucky because he had a mechanism to communicate and have others conduct the experiments.  Did we ever try to systematically conduct similar experiments on religious ideologies. May be yes and the ones who did them didn't care to publish a paper. May be we are still not there to conduct experiments on consciousness. The point being, this debate is circular in nature. And there is no end in a circle :-)

### Faith is a pre-requisite.

The more important point is "Do you believe in something?" . If yes, you are in the same camp. The other camp is who do not believe in anything. If you "believe" you are saying there is a question, and there is "a"  or "many" answers out there that need to be found. You can believe in "Evolution" or you can start with "Intelligent Design" ; if you conduct your experiments carefully, and assuming there is only one definition of "Truth" , all believers will reach the same conclusion. Non believers- well they won't even start on the quest till they start believing in something. Thus "faith" is a pre-requisite for the journey. It doesn't matter where it lies. 

Krishna puts this idea very eloquently in chapter seventeen of Gita. He says

>> Sattvanurupa Sarvasya, Shradha Bhavati Barata; Shrddha-mayo yam Purusho, yo yat shraddha sa eva sah.

Means the humans evolve their faith according their nature and nurture. We are believers and we are what we believe in. This also means that we are seekers and we always seek the truth. Which also means that any debate on "my faith being right and yours not" is pretty useless. 

### What we know for sure - starting point.

We know for sure that things work in unison in this perpetual universe. And most of it[universe] is well automated. The car runs: We only turn the ignition and press the paddle. This is not to say that we do nothing. We continuously expand the knowledge. Our purpose is to expand the knowledge. And the best way to do that is to first understand what we are extending.. 

The simple question that every rational being must ask, how and why all this is made available to us ? Who did it? We can name it an "accident". We can attribute all this to a chance. Or even call it "evolution". Those all are convenient terms to "black box" what we don't know. We can hide the unknown under evolutionary rug, but we just can't deny that we are very lucky to be here. Millions of cards must have fallen in right places. 

### Quest is eternal 

Long before modern science took current shape, humans asked this question. They asked why the sun shows up ? Why weathers change? We now have better explanations but as Richard Feynman once put it "why" can only be explained to a certain degree subject to the knowledge of asker and the explainer. At some point, after peeling few layers, we find there are infinitely many more layers and any attempt to further explain is counter productive. At some stage, we must accept there is more to it than we can fathom. The spirituals named it "God". The biologists call it evolution, the physicists and mathematicians have yet better name - they call it "probability".


The point here is we can't find the true answer ..Yet.   There is a journey we must complete before we get down to writing the answers. Gita says that journey is `Karm Yoga`.  
