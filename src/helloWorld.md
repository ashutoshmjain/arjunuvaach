# Welcome to the true [HelloWorld.gita]

First of all congratulations. If you made it thus far, that means 
- You are human.
- You are curious.
- You are willing to play your part - means invoke `Karm Yoga`.

And I promise, this is the most fun `HelloWorld` you will ever experience. In fact, you are already doing it. Reading this book is HelloWorld. Driving a car is same. Every time you make a choice, you are writing a new HelloWorld in this universal sandbox. You don't need any coding skills. You already have everything that you need. So we will use this space to dig a little deeper on the [protected] knowledge .. 

Lets address few simple questions. If I can already write HelloWorld apps, why can't I write the advanced applications?  Why doesn't things work exactly the way I want? Which all boil down to one thing .. 

>> Why this [protected] knowledge is so cryptic? Why I am not made aware of it right at the birth?

The answer is simple - we are!  We just need to brush it up. In fact it is like any other skill. Every skill that is out there; is already known to us. Remember the core design - we all get a full copy of the code. We just need to read the manuals and practice a little. That however doesn't mean, we are going to to do everything. No ! We will enjoy doing only certain things or a combination thereof - unique to each one of us. And that combination holds the key to  [protected] knowledge. Chances are you are very close to that combination. Your choices thus far have already brought you close  to where you wanted to be. You really don't need  to go to jungle to discover yourself but detaching yourself from material world wouldn't hurt. Taking some time to engage in some other activity helps .. May be you want to travel the world. May be you want to learn  Music. 

>> The process of learning is reading (or listening to) the instructions to practice something. That "something" is already known to us. For example, if you start your music journey with say Violin, the instructions are only to use the violin so that you can practice it. The knowledge of Music is hidden in that practice. 

Let us break it down further. When you say you want to learn Music, you are expressing an "outcome". A certain set of actions can accomplish this outcome. The actions being ...

- Buy a Violin
- Allocate two hours daily to learn Violin.
- Find a good teacher or look up a good lesson series on You Tube.
- Watch the series first 30 minutes of your allocated time, and memorise the instructions
- Practice the instructions for an hour and half - the remaining time of your two hours allocation.
- Repeat this for sixty days. 

If you want to do above actions, Universal sandbox provisions the resources that you need. What it can't promise that at the end of sixty days you will be a master of Violin. If that happens, good for you. If it doesn't , too bad. 

So at the end of sixty days, if you became world's best Violinist , you will be elated. What if you couldn't crack it ? Music is just isn't in your DNA. You will feel sad ..right? That is because you have attached the notion of pleasure and pain with the outcome. What if your goal was NOT to become a Violinist, you just wanted to play it for sixty days - just for the heck of it. Will you still feel the pleasure or pain ? You just wanted to play it and you played. Look at the children. They just want to play in the evening. No weight loss goal, no fitness mentor. They just want to play .. When did we lose that fun with the actions. 

>> Actions need NOT be always attached to an outcome. Acting without an outcome in mind is possible. We did it as a child. Most of the animals do it everyday. 

Now if you are too full of yourself, you may ask the question " What did universe do in it ? " I bought the Violin, I read the instructions, I played it .. wait .. you probably missed the previous chapter. You only paid a paper currency for Violin. You will need to do a PhD in Music theory to make a one yourself, even then the chances are you wont be able to build every piece that goes into it. You will sure never be able to make the air that carry the sound from Violin strings to your ears. And I guess you can't make human ears or hands for that matter. All these are there for you, pre-created by the universal sandbox and free to use ..

You got the idea - The language that Universal Sandbox understands is that of "Actions". It DOESN'T understand the language of "OUTCOMES". If you want to do something, it is right there. If you want an "outcome" it demands a clear list of actions. If there is a mismatch, you are responsible for your code. 

## Karma and Yajna

The Actions listed above are called `Karma`. Anythings that you need to do to accomplish a goal are `Karma`. Doing them for sixty days (in example above) is `Yajna`. So actions and Practice. Let us take one more example to understand it little more ..

Lets say you are hungry. The way you express it is "I want to feel full". This translates into set of actions 

- Order groceries and vegetables that you want to eat
- Get the deliveries
- Read cooking instructions or check out a recipe on Alexa.
- Cook the food as per instructions
- Eat it. 

All these actions are doable. Our Sandbox knows how to respond to them. It doesn't know if you will feel full after all these actions. Chances are you will. 

>> Gita says - You only own the Actions. Outcomes are beyond your control

Let us look at the Music example once again. Each of the actions can be further broken down. Lets take the first one for example sake 

- Buy a good Violin
    - Allocate say 1000 dollars for a Violin
    - Check with friends or consult online forums to find which is a good Violin brand and check the price.
    - Go to Music shop to test it 
    - Check the price online
    - Order online or pick up from Music shop where ever cheaper
    - Get the delivery.
    - Unwrap the Violin - you may wanna celebrate start of your Music Journey by making an Violin Unwrap-ing video :-)

>> Gita says - As you indulge in Actions, The `Karma` gets more and more accurate. It gets more detailed. 

Next time you need to buy a Violin, you already know lots of steps. It will take you lesser time. Next time you pick up the Violin to play, you will not need to see the instructions. Next time - after sixty days - it might sound like Music - No guarantees though :-)

## Templates and Expectations

Millions of such action lists are already known to us. You don't need to look up a manual to get your heart pumping. A child already knows where to look for milk as soon as she is born. We know that water will quench our thirst. We know we need fresh air to breathe. We already know where to find the food to eat - in the refrigerator , duh :-) 

We expect certain things to work - iPhone should just work; car should just work, hot water should .... You know. We expect universe to provision the things as needed. It does as long as it has a Template. A well defined action list - granular enough to act. If the action list is detailed enough, it will provision those things for us. This also means that someone has created those templates. 

The problem happens when we are doing something new. Because we are used to templates, we are trained to express our expectations. We got used to talking in terms of "Outcomes". We became consumers of the system, NOT developers. That is fine as long as we want to consume standard big Mac. But if you wanna make your own custom burger - the damn thing doesn't work.

The problem is want our unique thing but we only know expressions or expectations. We don't want to spend time in creating (and practicing) our own action list. 

## So what is your first "HelloWorld App"

Pick up a thing that you want to do. May be you expect to travel to some place where no man has gone before. Well that is a tough one. Why not grow a kitchen garden in your back yard ? Or foster a dog from the nearby animal shelter ! 

Write down the actions and check if Universe helps you take those actions. If it didn't , go to next level of actions and keep on breaking down till you get the response .. I am sure Universe is kind. May "Krishna" be with you !




