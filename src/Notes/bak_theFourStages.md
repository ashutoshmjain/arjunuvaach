When we start the work, we watch it. We explore around it. This is the first stage. Krishna says that our `Atma` always sees all our activities. Nothing is hidden from ourselves. Think of Krishna as the the conscious element in all of us. The one that gives us ability to think, evaluate the next action and allow us to engage. At this stage that being is watching. 

As we engage and continuously practice something. Any task. Good or bad. He then starts talking to us. He tells us if we are on the right path or not. He brings the new idea , newer pivots and new paradigms. This is the stage when he acts as a friend. 

The third stage, when we have attained perfection in our actions, the actions start bringing the pleasure. In this stage he says that I enjoy your actions. 

In fourth stage, when actions are so perfect that we don't even need to think. Then he says, there is no difference between you and me. Every thing is automated. You are acting like me. The God. 
