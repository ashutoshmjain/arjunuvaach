Before we get into the details of framework , lets address a simple question. Why this [protected] knowledge is so cryptic? Why do I need to go through the pains of "actions without desires [AWDs]" to understand this ? Shouldn't this be fully known to us right at the birth ?

The answer is simple - it is. We just need to brush it up. In fact it is like any other skill. 

>> Every skill that is out there; is already known to us. We just need to read the manuals and practice a little. That however doesn't maen we are interested in all. No ! we will enjoy doing only certain things or combination thereof - unique to each one of us. For that uniue combination holds the key to your [protected] knowledge. 

Lets say when you wanna start learning to code, you don't start with designing the libraries. You probably don't even do a "Hello World" application because you haven't yet decided what language to learn. So the first thing developers do is to provide the prospective users (or learners ) with some killer applications. For example, I am writing this book in an app `mdbook`. This a small beautiful app in popular programming language `Rust`. Using this app, I am already falling in love with Rust. The next step would be to open the help manual and do some quick start tutorials. May be write "Hello Rust". If it sticks, I will read more. And with luck, I might become a Rust developer. Or I might go to some other language that may offer easier onboard because Rust is by design a very detailed language. Same goes for the [protected] knowledge. We are first given apps to try out the beauty of this universe. We are given a car to play with it. Start the ignition. Make some choices. Press the paddle and take a ride. If we like it, we can possibly open the hood to check if it runs on gas or on batteries. Fortunately and unfortunately, this platform is very detailed. Millions of more time than any language we created. Fortunately because it provides us the experience we have. Fully immersive reality. Unfortunately because it takes times to wrap our minds around it .. 

# Survival first 

"Takes time"  means we need to survive till we get some insights and fall in love with our inner app [protected] knowledge. That is the reason our brains are tuned primarily for survival. In animals, there are not enough neurons to ensure survival and then devote some to learning. Luckily we have enough. We have already figured out survival. We have risk free trials on as many cars or gadgets as we want. Unfortunately, in our case, we have so many options that we just get lost in trying out the fun part. We rarely get down to the "Hello World" app of [protected] knowledge. The options that are given to us to exhibit the power of this design, turn into the "Matrix" around us. 

# Pleasures 

Not only time, we are also given curiosity. When we are born, we are made curious to search for milk for mother's milk is the first thing we need to kick start this machine. The  curiosity is in built. In addition to curiosity, every aspect of the environment filled with pleasures. Just to keep you hooked. When we get hungry - food is a big pleasure. Water is even bigger pleasure to the thirsty. Not even talk about chilled beer :-) Even defecation is pleasurable. Talk to someone who is constipated. And then we are given the biggest pleasures of all - you guessed it - love and sex. All these just to give you a glimpse of how powerful is this language. It is definitely as good as Rust or Go  :-)  and is sure older than C :-) and is sure older than C :-)  ..don't you agree?

# The Cost of the playground 

What happens if billions of us keep asking for more and more pleasurable constructs without contributing back?  The system - collective consciousness is obligated  to provide us all that we want to figure out our "Hello World" . In fact it provides to all the animals and all the species who don't even have the capacity to learn or contribute. They don't even have dollars !

Every time we ask for a new free ride, we are putting pressure on the resources.  Every time we desire something, the universe makes attempts to fulfill as quickly as possible ; provided the desires are legit (and communicated clearly). If billions of us continue to do that, at some point, system would crash. As we are already seeing it with our beautiful planet. We have filled it with garbage. Burnt its natural resources. Extincted the jungles and the species that live on jungle. Air is hard to breathe in big cities. And now we want to move to the Mars. Even that is granted, provided we start paying some dues.

# Didn't I pay with my USDs and BTCs ?

Yes you did. Everything you shopped at Amazon, you paid with paper or crypto money. But does it actually pays for the all that universe provisions - including the water, air and this beautiful body it self ?

In fact, money and trade is just another multi user game that we can play to consume ourselves when we are done with the high school games. Counting your beans is as pleasurable as the hitting the post in basketball. It is a game of old and sad part is older were expected to start contributing back to the actual system. 

# I told a lie ..somewhat 

When I said we don't pay for all the pleasures, I was little wrong. We do pay with the pains and miseries. For every pleasure, there is pain to follow. That is what makes it cyclic. Pleasures of more money, more success are always paid back with more sorrows. For every rise, there is a fall. Richest of the rich can't escape it. Even stars go through the same fate. This is an eternal reality. The obvious solution then would be to not play the game at all. Give up everything. But here is the bigger misfortune - once you enter the game, the exit is only after solving the puzzle. There is no way to switch off this reality. Not even with death..

>> Gita, doesn't change this truth. It only gives you a path to sail through .. It is the true "Hello World" tutorial. 

Well, this should not surprise anyone. We should not conclude that this is a unfairly rigged system. We have tutorials, docs and contextual help. And if you think about it, even in our humane systems, there are penalties for not following the code of conduct. For example, lets take social media, such a beautiful thing but then people misuse it. They build bots to alter the elections. There is every type of hack in play at every level. Sometimes when the things get out of control , what does Twitter do ! .. They are forced to kill the account of most celebrated celebs. 

Krishna said the same thing long before we found the Facebook terms and conditions.. He said 

>> Yada Yada hi Dharmasya , Glanir Bhavati Barata , Abhuthanmdharmaysa , Srijamyaham Yuge Yuge. 

Don't worry about the Sanskrit though it is considered to be on top of most scientifically designed languages.. at least till c was written :-) . What Krishna means here is - Whenever there is too much hacking on code of conduct, I incarnate myself as a human to clean up the system". See the beauty of the system - Even to collective consciousness provisions all the experiences but it can only act through a regular account - just like you and me. There is no deletion of data from the back end. Yes, Krishna has the root access, but she is still a regular human user. You and me can also get the same privileges. In fact the purpose of Krishna is to create enable users like Arjuna to clean up the system. 
