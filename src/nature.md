# Nature

In last chapter we looked at conciousness and how it develops from practice `Yajna` and leads to manifestation. 

Krishna says that he created the code for `Yajna` and first told to `Prajapati` - the one who is responsible to take care of masses. Why did he demanded practice ? Simply cause work is the cost of the things you manifest. Is that why bitcoin block-chain demands "Proof of Work" :-) Now here is the bigger one - Manifestation is what gives us notion of time - `Kalpa`. But lets not get ahead of ourselves. Let  us first look at the nature. 

Let us say, you turned sixteen. Yes there are many things going on on Snap-Chat but the day finally came when you will be allowed to take the driver's test.  You are excited because you want that yellow car , you know why ! - let us not go there :-). You did well on written and then took fours driving lessons "behind the wheel". You knew this already but finally world knows that  you nailed the driving test. A big day ! The news is on Instagram .. But dad is still acting weird. Mom is still not ready  to let you drive alone. And so is the law. You will need to wait till eighteen to be on your own. Why so - you ask your friends - sucks. This is an Old Man's world. 

Anyways , you are driving on and off. Sometimes mom to do groceries. Other times with dad to take a drop to school. Your as well as their confidence is gradually increasing. Dad still keep on his lecture on traffic rules but he is kinda slowing down. And finally one day, he threw the keys to you. You are on your own and probably 18 too. World is below your feet - "in the gas paddle". You can go where ever you want. You can take anyone with you. The feeling is making you restless. The freedom can't be put in the words. You talk to your friends like an expert on "Internal Combustion Engine". You explain how the crank shaft works. But you lose words when she passes by. You let your car do the talking :-) And she knows what you said ..

Couple years passed. Now you drive to work. You already have your own car - no you didn't buy that Yellow one :-) But driving is like second nature now. You don't even feel it. It just works. It is in your **Sub-Conscious**. It just became part of you. Your feet know when to accelerate and where to stop. Your hands steer in one decisive motion. There is no planning needed. Eyes , hands, feet and the machine - they all work as if just one entity ... Just like the Sun shows up , Earth goes round and round , weathers change , wind blows .. 

>> Nature is "manifestation" and "sub-conscious" at play. When your sub-conscious is chewing food with annoying sounds - it is your nature. When Sun is on the shining shift - it is the "sub" of collective consciousness. You become a driver when you forget driving. And "Nature" is beautiful because it is who we are !

## Always at Motion 

Nature is continuously at motion. Rivers are flowing. Wind is blowing. Trees are growing. Leaves are falling and then new ones growing again. You driving. And the Music - the one you learnt for ten years on your Violin, feels natural when it comes from sub-conscious. It all seems effortless. The work was only to move something from conscious to sub-conscious. Once it is in "sub" zone - it just happens. 

>> When Krishna says - I am the Doer - This is what he means. The collective consciousness - which is essentially the "sub-conscious" of entire universe, provisions everything. It provisions Violins, It provisions the ears to listen , it also provisions the air to carry the dance of strings in musical waves .. An effortless symphony of timeless motion.

And the beauty is, it all happens instantly. Just like the well written code executes in no time. It does what it needs to and exits. 

## The interplay

Again - let us hold the "Chicken and Egg" question - Consciousness is what indulges you in 'Yajna'. Yajna leads to Manifestation. Manifestation is in motion with the sub-conscious - that is "Nature". And consciousness arises in Nature to do something more. There is no notion of who came first. This all holds each other. It is a circle that gradually expands from nothing to Gargantuous universe. 

Gita calls it the play of `Prakriti and Purusha" - Prakriti is  Nature , Purusha is the consciousness. 

# The time 

The time is the duration of `Yajna`. The time it takes from init of practice to the culmination of manifestation. It is called `Kalpa`. A `Kalpa` may include many `Yajnas` till a self sustaining module is ready. A `Kalpa` may also represent creation of entire universe. It can be a split second or millions of years long but that is how we measure the time. We will discuss our distortion of time (on acount of senses and desires) later but in nature there is no difference in a split second or millions of years. If there is no new initiation of `Yajna` - called `Sankalpa` ; there is no passage of time. That lends perpetuity to universe. 

`Yogies` - One who can practice being absolutely one with the nature (manifestation and sub-concious) can stop evocation of `Sankalpas` and go into a timeless `Samadhi` but that is a different discussion . Suffice to say here that stopping `Sankalpas` and trying to be in symbiosis with nature is what we call call Meditation - `Dhyana`. The finest stage of meditation is to stop consiouness and allow universe to fill you with free flow of knowledge - `Nyana` that you can later (if you choose to) use to consciously manifest. 


# Silence

One of the ways to get into `Dhyana` is to listen the sounds of nature while all other senses are at rest. There are no `Sankalpas` rising in your conscious. There is nothing to be done. This is not just the silence of speech. It is silence of everything except for the ears that are universal antennas. We call it Music of the nature. 

 `Yajna` and `Dhyana` are complimentary to each other.  In a way nature forces it on it. A good circadian sleep is `Dhyana`. Your ears are open but all other senses are at rest. You are are not actively engaged in any practice. When you get up, you are filled with universal wisdom as much as you can hold and with that you persue next iteration of your `Yajna`. One reason to perform your targetted practice as early in the morning as possible. As the day passes, your actions get lazy and incoherent. You spend too much time without manifesting much - diminishing rate of return. Finally body says enough - time to leave `Yajna` and get into `Dhyana`. If you must do something in later part of the day ; try to fisrt block 15 to 30 minutes for Meditation. Let your mind relax in silence and then take on next action. Best is to follow your circadian rythem. Your body knows when to get up and when to go silent ... 


-----

All that I am saying here would make little sense if you don't do it yourself. As I said Gita is experietial. So here is a small exercise to get you started .. on the path to opening the doors of [protected] knowledge. 

# Exercise 1 - Voice of the nature ..


Practice `Dhyana` first. Sit in open. Your back yard or roof top. If you have none of them, open a window and face the window. Sit not too high , not too low , say a foot above the ground and close your eyes. Watch your thoughts disappear and try to open your universal antennas - ears. Start picking up the sounds you hear. Is it a dog barking? Some vendor on the street trying to sell his wares. Or just the noise of wind ? There must be sounds. Something is there. Focus on the sounds. 

If your mind wander, ask it what is your 'Yajna'. What is it that you want to start with coffee tomorrow morning. Do you want to sing ? Do you want to paint, code or write? It should be something creative. You wanna come back to it in the evening and feel good that you manifested something. 

That is it .. Only caution - don't share it. Keep your smart devices away. Just do it for yourself never to be shared with anyone. 

Bring your conscious back to sounds. See if there is a rhythm . I promise you will find one. May be on your third attempt. Keep trying till you unlock the voice of nature 

---
