
# The Hypothesis

Gita proposes a simple hypothesis to help us understand the nature of our reality and a framework to exit the maze. It is NOT wrong to call it a maze because we have little understanding how all this works. It looks similar at sub-atomic level as it seems at the astronomical scales. We can't say (with confidence) as to what transpired at the beginning and how this all is going to end. The questions of physics are still trivial  compared to emotional problems we face. Despite all the material consumption, we are not happier than before - evident in substance abuse and suicide rate. An average American family spends more on meds than on food. 

The Gita Framework resolves many of our burning questions, or at least attempts to set priorities as to what we should strive for. The later is more important because even science appears to be all over the place ; least said about religions. In a way Gita, gives necessary structure and skeleton to our quest - scientific and spiritual. 

### A distributed model of knowledge

It proposes a simple distributed model of knowledge. 

>> It says that a full copy of  [protected] knowledge `Atma` is in-built into us. We being the conscious agents. Protected because, this "knowledge" can not be destroyed by "worldly" means. Fire can't burn it. It can't be drowned in water ...

At the risk of analogies, this may be seen as DNA on one hand that carries the native code of manifestation and ELECTRICITY on the other, that lends the energy to our "Actions". 

Thus every one gets the full instructions for manifestation and energy to act on them. Which means every conscious agent has the potential to be whatever they want to be. 

Our Actions `Karma` determine what we finally shape into from our physical body standpoint and what we manifest in the world. We have full control on our `Karma`. 

### What we should aspire to know and how?

Gita says, this [protected] knowledge is all we need to know. It adds, the answer to all the questions that we are seeking outside, lies with in us. Since it is  built into us, it is called `Atma` which means "Me or Mine". In essence, this is what we are. We are NOT the physical body (or mind). Our material body is like the clothes. We can alter our physical shape as we want to and it anyway decays with time. 

>> This knowledge is easily accessible  to us though it appears "encrypted" to our externally focused senses. 

The easiest way to unlock this knowledge is through observation of "Actions". As we bring our focus to the actions (not the outcomes of actions), the knowledge starts revealing itself. 

### What is `KarmYoga` ?


Performing righteous actions rigorously - like a ritual , is `KarmYoga`. 

The caveat, however, is - our focus on actions needs to be such intense that we forget  the "Outcomes". It might appear unreasonable to modern goal oriented thinking .

The way to reconcile this conflict is to think that the goal of `Karmyoga` is to understand the [protected] knowledge. The only way to observe this knowledge is through observation of actions because that is the only accessible element of [protected] knowledge to our externally oriented senses. And as we start observing the actions, our focus must shift from "outcomes" to "Actions". In essence we must seek pleasure in "Action" NOT in the "outcomes". Outcomes will be there but they are to be treated as by-products. The primary goal is to know yourself. 

### Why righteous actions ?

Any action is good if the goal is to only know / understand [protected] knowledge. Doing "righteous" actions is a smarter way because the people around you will not only allow you to devote time on such actions, they will even facilitate if needed. .. On the other hand, if your actions are not righteous, people will hinder your ritual. As a `karmyogi` , you don't care about the outcomes, but people around you do. So ..do the right thing:-) .it is easier ..

**Thus Krishna said to Arjuna ..**

>> If you win (in realizing your [protected] knowledge, you will exit from the miseries of the cyclic nature, even if you lose , you will still have the wealth of this world to rejoice. 

# Elegance Test

Before we pursue the experimental test of the hypothesis - the idea must pass the elegance test. The model proposed by Gita is beautiful in the sense it uniquely empowers every individual to overcome the miseries of life and at the same time, it sets up the stage for a successful career as a residue. Our individual success leads to a better collective consciousness and thus a world worth striving for. 

In essence - if Gita is True , it is a good news we all are waiting for.. 

Let us start the journey .. 
