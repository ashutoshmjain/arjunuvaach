# Gita

Gita is a conversation between Krishna and Arjuna. In the larger narrative `Mahabharta`, the legend says , Krishna is the incarnation of God - as a king, a diplomat and a friend and the one who stands up to the evil to restore the order;  but his role as the driver of Arjuna's chariot is the one that is contextually (and probably otherwise), the most important one. Driver of the chariot is a symbolic presentation of someone who is leading you on a righteous path; particularly when you are up against a major dilemma. That makes Gita  useful guide rails to navigate modern life. 

##  Key Observation - The cyclic nature of our Reality. 

The birth and death are cyclic so are the day and night. In observable universe, everything grows and diminishes. That is the nature of our reality. Greater the rise, bigger the fall. Every success finally meets it's failure. This is as true in business as in our emotional lives. Happiness and sorrows are a never ending cycle. Planets, stars and even black holes can't escape this.

Krishna, in said conversation, lays out a new methodology to help us stay afloat in the waves of joy and grief. A method that was unheard till he said it and remains unique to date.  Krishna's message of `Karm Yoga`[Righteous Action]  is simple to understand, easy to put in practice.  It not only offers exit from cycles, it also makes the follower feel "blessed and blissed " which obviously translates into worldly success.

## Religion Agnostic

Despite being part of Hindu scripture (s), Gita doesn't belong to any religion nor does it advocate a specific God or a specific way of worship. In fact, it strongly rejects the popular religious ideas. The situation it self is very symbolic - You have a king talking  to a prince in the middle of the greatest war. There are no long bearded monks here. This discourse is NOT at a Himalayan cave . In fact , it is not even a discourse - it is a one on one conversation. And the out come of this conversation  is the biggest blood shed ever known to mankind - at least, till the time of its writing.  There is nothing religious about it. All it says is **you got to do what you are made to do**. The key lies in the fact that we often don't want to do what we need to. Sometimes we are swayed by the anger, other times we are dismayed by the fears . Being able to perform our `Karma` [Righteous Action] is difficult but it is a necessity for the order. And Krishna says, it is also the exit from the cyclic nature of our reality - thus the path to peace. 

# A modern way look at Gita

At the time of its writing, it was probably a good idea to pass on Gita as part of scriptures, using fiction as the conduit. A lot has changed since then. Today, technical literature is equally palatable - if not more. Which is kind of sweet spot for Gita. It is not to say, Gita is in bad company when it is taught by spiritual `gurus`. It is just to add that a much broader channel is needed for such a scholarly piece of work. 


## Message is lost in Translation(s)

There are umpteen numbers of scholarly texts written on Gita and they go to great length in differentiating `Karm Yoga` from devotional services `Bhakti Yoga` though Krishna says `Bhakti Yoga`, and quest for the knowledge `Nyana Yoga`, both lead you to the same conclusion. However, keen readers of such texts often get lost in contrasting and evaluating various modes. It gets hard to ascertain if I am a `Bhakti Yogi` or `Nyana Yogi`. We tend to easily ignore the core message - `Karm Yoga`. Krishna , in his tweet size verses, says all the `yogas` are essentially enablers. The key thing is to do your work without getting attached to the outcomes. A 180 degree opposite view to our modern way - where we work towards the goals. Krishna says 

>> You only own your actions. Outcomes are not in your control. An intense focus on [righteous]"Actions" leads to success in this world and exit from the misery of cycles.

The unique thing about Gita, unlike most scriptures, is it is based in sharp logic and is "to the point". There are 700 odd verses that are probably less words than a normal college app essay. But that doesn't stop it from taking a very deep view of the nature of our reality. Unlike most scriptures, the messages in Gita do not ask for a blind faith, instead, they provide a very objective framework to appeal to rational thinkers. 

## A different way to communicate

With  advent of printing press and online tools, Gita got translated in almost all the languages - written and spoken; yet the style of delivery is same old. It works well for religiously oriented older populace but Gita is needed most with in the younger circles. The younger generation is adept at consuming not only media but also large doc sets from subjects as complex as Artificial Intelligence to Gnome project. This work is to approach Gita as technical documentation for a complex multi-faceted app - The Reality. 


# About this work

This work comes from an old habit of mine. When I listen some one explain stuff to me, I tend to say it back in my own words to get a nod that I really understood . I think it is a good practice for one, it completes the communication loop and second it often leads to better understanding.  So the narrative ahead is me listening to Krishna and speaking out my understanding as Arjuna. Thus the name `Arjuna Uvvach` which means "Arjuna Said". 

My goal is to say it in most accessible way. I find the way we write technical docs (for complex applications ) is best suited for such a logical and purpose oriented text. The goal is to get something tangible for the time spent by the reader. 

My hope is, in the process, I will be able to separate the fluff of all the #Yogas from the real one - the `Karm Yoga` and break it down in a rational doc set that is beyond blind faith and is logically appealing. The work here is not mere reflections of what I read, it, in fact comes from the me putting the ethos in practice to whatever little I could. I don't claim `Nirvana` but I vouch I have seen the tip of iceberg. Peace and happiness were alien to me. I was a wandering soul ever so lost in "what next" and consuming my way to bloat. Gita has helped me focus, remove the unnecessary baggage and fine tune my life. And it remains a work in progress though I see a light at the end ... 



~Ashutosh~
