# Consciousness 

The fundamental difference between conscious and unconscious is ..well - conciousness. But what does that mean? I think we need a solid grounding on consciousness before we humor to wrap our minds around collective consciousness.

Religion and Science, both grapple with this question and frankly there is no agreed definition. The reason is simple, despite being conscious , we really don't know what consciousness is. If we can't perceive something with our senses i.e. see it; smell, touch, hear or taste it, it is difficult to explain that thing to someone - only through attributes. For example, it is quite difficult to explain colors to a born blind person. Important thing is, the other person should also have the same sense enabled. . How will you tell the difference between jazz and country to a born deaf? 

## Is it a mistake ?

This seems like a big evolutionary mistake. Evolution (or Intelligent Design) has given us to senses to see , taste , touch , hear or  smell ; every thing that is important to us- at least from survival standpoint. Why then we can't perceive consciousness. Is it NOT important enough? I guess a better question is - **how important is the survival ?** There are very many super important things that are out there and are beyond our sensory perception - for example we can't perceive earth's electromagnetic field. We can only see a narrow band of light what we call the visible spectrum. We can't hear what bats can. Thus being beyond senses is NOT being unimportant. Right ? In fact most of human contributions are when we go beyond our senses - be it space time or quantum mechnics. Be it computers or synthetic biology. Every time we notice that our senses are limited to survival that we have already nailed - to a great extent. That could very well be the purpose of humanity (v/s animals and other life forms) but let is NOT jump the gun .. It could very well be a double edged sword..

## Lets try  to define consiousness. It is NOT hard ..

Is there something that we can all agree upon. That common characterisation may help us formulate an acceptable understanding. In fact when I and you , both with good eyes, agree on a color --say green, it doesn't mean my perception of green is same as yours. You know your green, I know my green. We both agree to call it "green". If you call it Brown and I call it green, then there is a conflict; otherwise both of us are happy. Thus the important thing is not how it appears to us, the important thing is we all call "winter" the Winter. And we all agree on August is Summer - well at least on our side of the globe. So you may feel consciousness looks like a cat, while it feels a dog to me ..let us just find the things that work for both of us .. 

- First thing we did agree upon is that it is beyond our senses. It also appears to be  beyond any of the instruments that enhance our senses. Means there is no hearing aid that can help up listen to consciousness, there is no such telescope, there is no measure for the consciousness field that can turn it into something our senses could interpret. For example we can sense an electro-magnetic field with an EMF meter even though it may be beyond our senses. Thus consciousness appears to be beyond the realm .. yet we are conscious.
- Someone can be knocked unconscious if they consumed too many martinis :-). At first we lose the control of our speech ; somewhere between first and second, the law prohibits driving a machine which expressly includes a "car from the bar". With third large down, our legs start dancing and with fourth; those very legs cave in .. Okay, it could be fifth in your case, but the point is  - there seems to be different levels of consciousness. Just like different shades of green. I can be less conscious than you. Some people are quite conscious in there sleep while others just lose it. What about animals - Are they conscious ? Yes but they can not solve a quadratic equation. Is consciousness also related to intelligence ? Can we be more conscious at the start of the day v/s the middle of the night ? We are definitely more conscious before a large burger and a big jug of beer than after :-) . Bottom line - there seems to an intensity of consciousness - Sharp to  blunt. 
- Consciousness also guides us from wrong to right. It may appear logical to not pick up your dog's poop if no one is looking but something in you tells you to do it anyway :-) ..Isn't that consciousness ?

>> You got the point - I guess I am driving you to a conclusion that our individual conciousness is something that organizes our little world for us. It not only operates our limbs, it also drives our moral standards. It is as much a physiological phenomenon (ie the neurons in brain) as is the result of our culture and value system. And most importantly - consciousness has different levels. Thare are certain things that happen sub-consciously and then there are other that come with great cognitive workload (such as reading this page :-)

## Expansion of Consciousness

If above is true, there must be ways to enhance our consciousness. Can I combine my "consciousness" with you? Can I expand it to connect with animals or plants? Can I connect with or tap into universal consciousness ? Those are exciting questions; aren't they? ...... But the most important one is "Can I get rid of the pains and sorrows of my this life" ..Can I be in state of eternal peace ? That seems important, because if I am not at peace, what is the point of expanding my consciousness ? I should not expand pains and miseries - Isn't it? 

### Paradox

The paradox thus seems to be .. 

>> I want to be at eternal peace so that expansion is meaningful. But if I am at peace, why would I change the status quo ?

Which means, the end to all the expansion of my consciousness - looking outwards ; lies in me being internally fulfilled. But being internally fulfilled makes external vantage redundant. The question really is - How can you be at peace and still want to expand your conciousness? The answer probably lies in just focusing all your attention on one thing rather than jumping from one pillar to another post. Let us check this out little deeper ..

# Invoking Consciousness

We agreed that we can't measure consciousness but is there a way to at least feel it ? How do I say I am conscious ?

Let us tread this through our old Violin example .. I love Music :-)

## Action

Let us say you bought the finest Violin and you also bought tons of Music books , signed up fifteen different Violin courses on Youtube or elsewhere. Can you say you know music? ...I guess "NO"....No matter what you collect, the music comes only if you pick up the Violin and practice on the damn thing. Even then there is no guarantee. 

Let us consider one more example from the world of computers - I love computers too :-) 

When you install a program - you get the code of the program loaded on your computer. If it is open source program - you can look at the code any time. For example if you installed Linux on one of your old machines, approx. 27 million  lines of Linux code are available to you right on your machine. It also gives you all the documents that you need to understand that code. Lets say you downloaded source code as well as docs. Does this mean you know Linux? You may know how to use Linux but if someone asked you how and why it works, you probably has no answer. I suspect if even Linus Torvalds (the guy to made Linux and gave it free - yes there are such crazy people even in these times) , could say he knows all the code because thousands of developers are working on Linux. 

So what does "knowing mean" ? 

You need to pick up the Violin and practice on it to know Music. You need to pick up an open issue in say a tiny module of Linux and try to fix it to know a teeny tiny part of Linux. 

Do you see the point ?  - It is the appropriate action that makes you know the things. If you bought a Violin - yes that is an action too but it only lets you know how to buy a violin. If you use a program , that action lets you know how to use the program. 

*The world is full of musical instruments. It has all the tools and tutorials to learn the Music. But unless we do it, we are only consciously consuming the music on Spotify. We are pretty much unconscious when it comes to creation of Music.*

Can we say - Action is what makes you conscious ? Reverse may or may not be true. 

**If you agree, I guess you will start seeing that Actions are the way knowledge manifests. And manifestation is what consciousness does.**

## How much Action?

Continue with Violin , couple days may not cut it. A week might let you play the basics. A month of daily rigor, you start to see the sounds. Six months - you can now play on demand. Six year - you create your own tunes. Ten years - Violin doesn't matter anymore; you know the Music. Instruments fade away in the back ground. Now you don't need to make an effort because pleasure is built into the practice. You enjoy the waves of the notes. You are a "conscious" music creator. Your consciousness has expanded. 

>> Music is your `Karma`. And practicing it is your `Yajna`. 

## What is my `Yajna`

Each one us has a unique combinations of skills. Gita calls is your `Swabhava` - your nature. It actually goes a step further - it says "You can not stay away from performing your `Karma` because your nature will anyway pull you in. It is up to you if you want to make it `Yajna` and thus realize  peace in your consciousness while you are ever expanding it.

# Duality

As soon as we take actions and practice something ; there is a cost attached. It means you are missing out on something. The first question that comes to the logical mind - am I on the right path? Even if we hush this question, it seems like  a fact that there is something else out there that could be your calling. Why "INVEST" time here while you could be on Twitter. Your fans need you on Instagram. You have an obligation to your whatsApp friends. All valid and real points.. Duality is our nature. 

Which means , there are two things - One you are trying to be conscious of and second that is falling off your radar. This is the basis of all duality. If you are awake in the day, the Night is what you are missing out. Dark is where you are not shining your consciousness. 

## What is ONE ?

When you have put in ten years into Violin, you are now no more a Violinist. You now know the Music. Ten years into coding, you are now no more a random developer. You don't say I fix XYZ module. You say I do Linux. With your tiny piece of expertise, you are now connected to the larger and yet larger entity. That is the accomplishment of `Yajna`. 

>> `Yajna ends the duality. And makes you one with Collective Consciousness. 

And you now do it with peace. In fact, peace is absence of "FOMO" 


# Welcome to the Framework ..

The framework of Gita, is a path to bring our consciousness to peace. I didn't say "eternal peace" because that is the end goal. The path leads us to the goal. But it is not a straight easy path. A word of caution though - this path might appear 180 degrees opposite to what we learn in this survival oriented; "FOMO" driven, dualistic world. 

Krishna says in Chapter 2.69 , 

>> Ya Nisha Sarv Bhootanam, Tasyam Jagrti Sanyami; Yasyam Jagrti Bhootani, Sa Nisha Pashyto Muneh |

Which means- In the dark night of all beings, awakes to light the tranquil man. But what is day to other beings is night for the sage who sees. 


