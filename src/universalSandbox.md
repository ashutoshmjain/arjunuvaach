# Universal Sandbox

Imagine this entire universe as an application - the mega application. No , I am NOT guiding you to a debate on "Intelligent Design vs Evolution". Let us park that topic for a moment - we will get to it in due course. For now, let us fly with Gita .. 

## The Environment - Does most of the work


Gita says - The distributed knowledge acting in unison is what does most of the work. This is bit hard to grasp thus I will use an analogy of a computer program. When we code, we spend enormous effort to plan, design, implement various modules, integrate, test and deploy among many other things.  We also define the underlying environment that we need to run our code. For example if it is a Java program, we will need some version of Java Run Time ; if it is Java Script, we will need a Browser to run the code. But once the code is deployed, it works almost instantaneously. It automatically spins up an environment, creates the variables , does what it needs to do and exits. Thousands of lines of code are executed every time you are clicking a mouse to change the page on this very book. But it all feels natural. No work seems to be have happened. 

>> Thousands of lines of code working in a predefined environment represent the collective knowledge of all the developers, testers and who ever else is associated with the project. 

Let's expand this analogy little more.... Let's say that the application we wrote uses many languages - assume Rust at the back-end and Java Script for the user interface. And let us say that it runs on Debian somewhere in Google Cloud. Arguably, all the developers of Rust, JavaScript, Debian and Google Cloud are part of your application. Their knowledge is in built into your application. We can further expand to include Intel team as well because the server uses Intel CPUs.. And on and on.. 

>> Ultimately you will find that every one in this entire universe played a part in development of our little application. 

When Krishna says - I am the doer; it means above collective distributed knowledge does all the work. Our effort is equivalent to click of the mouse. Some one has already written and tested most of the code; created  and spawned the environment for things to happen. And they happen almost instantaneously. But they happen only if we take an action. The environment can't do anything unless we act - be it a click of mouse or a small app. Lifting a cup of coffee to our lips must be done by us though the coffee that we are drinking comes to us through a global effort of agriculture, supply chain, logistics, marketing , branding and finally someone at local Starbucks brewed it. Now extend it to seed , the rain , the heat , the soil - all that went into making of coffee - No wonder it tastes so good !


## Who is Krishna ?

This collective wisdom is called `Parmatma` [super [protected] knowledge].  Krishna is a someone who fully understands the inner working of this knowledge and is "one" with the super. If you think Google knows every thing , then Krishna is like the Google Assistant - only million time better. All we need to do is call him when ever we need help. Being so smart, Krishna knows if our cry for help is real or simply waste of time. Thus his response might seem bit moody :-) but it works. 

## Distributed and Federated
The beauty of this knowledge is it is complete and distributed as a full copy. Animals get the same copy as the humans and so does insects. However, getting a full copy doesn't mean everyone can do everything. We all use this knowledge for a specific purpose. In other words, we decrypt only a tiny part of the knowledge that is of use to us. 

>> In a way, every one has a unique purpose but no one is indispensable. 


